package com.jellyking.mimosa.comicsextractor.data.repository;

import android.os.Environment;

import com.jellyking.mimosa.comicsextractor.data.manager.ApiManager;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;


/**
 * Created by arthur on 10/03/2018.
 */

public class FileRepository {
    private final ApiManager apiManager;
    private final static int BUFFER_SIZE = 8192;

    private String receptionFilePath;
    private String baseUrl;
    private int currentPageNumber;
    private String comicsName;
    private int numberOfDigit;
    private List<String> filePathList;

    public FileRepository(ApiManager apiManager) {
        this.apiManager = apiManager;
        receptionFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile() + File.separator + "comics";
        filePathList = new ArrayList<>();
    }

    public Observable<Integer> startProcessingListDownload(String url, int indexOfTheFirstPage, int numberOfDigit){
        if(createTemporaryFolder()) {
            currentPageNumber = indexOfTheFirstPage;
            baseUrl = url;
            this.numberOfDigit = numberOfDigit;
            comicsName = String.valueOf(new Date().getTime());
            return retrieveUrl();
        } else {
            // folder creation failed
            return Observable.error(new Exception(""));
        }
    }

    private Observable<Integer> retrieveUrl(){
        //increment page number
        String currentPagNumber = getPageNumber();
        this.currentPageNumber++;
        String completeUrl = baseUrl.replace("{#}", currentPagNumber);

        return downloadImage(completeUrl);
    }

    private boolean createTemporaryFolder(){
        File folder = new File(receptionFilePath);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }

        return success;
    }

    private Observable<Integer> downloadImage(String url){
        return apiManager.download(url).flatMap(new Func1<Response<ResponseBody>, Observable<Integer>>() {
            @Override
            public Observable<Integer> call(final Response<ResponseBody> responseBodyResponse) {
                return Observable.create(new Observable.OnSubscribe<Integer>() {
                    @Override
                    public void call(final Subscriber<? super Integer> subscriber) {
                        try {
                            if(responseBodyResponse.body() != null){
                                subscriber.onNext(currentPageNumber - 1);

                                // you can access headers of response
                                String header = responseBodyResponse.headers().get("Content-Disposition");
                                // this is specific case, it's up to you how you want to save your file
                                // if you are not downloading file from direct link, you might be lucky to obtain file name from header
                                String fileName;
                                if(header != null){
                                    fileName = header.replace("attachment; filename=", "");
                                }else{
                                    fileName = comicsName + "_" + String.valueOf(currentPageNumber - 1) + ".jpg";
                                }

                                File file = new File(receptionFilePath, fileName);

                                filePathList.add(file.getAbsolutePath());

                                BufferedSink sink = Okio.buffer(Okio.sink(file));
                                // you can access body of response
                                sink.writeAll(responseBodyResponse.body().source());
                                sink.close();

                                retrieveUrl().subscribe(subscriber);
                            }else {
                                try{
                                    zipList();
                                    deleteTemporaryFiles();

                                    subscriber.onCompleted();
                                }catch (IOException e){
                                    subscriber.onError(e);
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                        }
                    }
                });
            }
        });
    }

    private String getPageNumber(){

        StringBuilder builder = new StringBuilder(String.valueOf(currentPageNumber));

        while (builder.toString().length() < numberOfDigit) {
            builder.insert(0,"0");
        }

        return builder.toString();
    }

    private void zipList() throws IOException{
        String[] files = filePathList.toArray(new String[filePathList.size()]);

        BufferedInputStream origin = null;
        ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(new File(receptionFilePath, "archive.cbz"))));
        try {
            byte data[] = new byte[BUFFER_SIZE];

            for (int i = 0; i < files.length; i++) {
                FileInputStream fi = new FileInputStream(files[i]);
                origin = new BufferedInputStream(fi, BUFFER_SIZE);
                try {
                    ZipEntry entry = new ZipEntry(files[i].substring(files[i].lastIndexOf("/") + 1));
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0, BUFFER_SIZE)) != -1) {
                        out.write(data, 0, count);
                    }
                }
                finally {
                    origin.close();
                }
            }
        }
        finally {
            out.close();
        }
    }

    private void deleteTemporaryFiles(){
        for (String filePath : filePathList) {
            File fileToDelete = new File(filePath);
            fileToDelete.delete();
        }
    }
}
