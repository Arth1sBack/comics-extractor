package com.jellyking.mimosa.comicsextractor.presentation.presenter;

import android.util.Log;

import com.jellyking.mimosa.comicsextractor.data.repository.FileRepository;
import com.jellyking.mimosa.comicsextractor.presentation.view.MainView;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by arthur on 10/03/2018.
 */

public class MainPresenter {
    private final FileRepository fileRepository;
    private final MainView view;

    public MainPresenter(MainView view,FileRepository fileRepository) {
        this.fileRepository = fileRepository;
        this.view = view;
    }

    public void startDownload(String baseUrl, int numberOfDigit, int firstPageIndex){
        view.displayIndeterminateProgressDialog("Please wait", "Downloading files");
        fileRepository.startProcessingListDownload(baseUrl, firstPageIndex, numberOfDigit)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Integer>() {
                    @Override
                    public void onCompleted() {
                        view.dismissDisplayedDialog();
                        view.displayErrorMessage("Download complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.dismissDisplayedDialog();
                        e.printStackTrace();
                        view.displayErrorMessage(e.toString());
                    }

                    @Override
                    public void onNext(Integer fileNumber) {
                        view.updateDisplayedDialogWithMessage("Downloaded files : " + fileNumber);
                    }
                });
    }

}
