package com.jellyking.mimosa.comicsextractor.presentation;

import android.app.Application;

import com.jellyking.mimosa.comicsextractor.data.manager.ApiManager;
import com.jellyking.mimosa.comicsextractor.data.manager.ApiManagerImpl;
import com.jellyking.mimosa.comicsextractor.data.repository.FileRepository;

/**
 * Created by arthur on 10/03/2018.
 */

public class CEApplication extends Application {
    private static CEApplication application;
    public static CEApplication application() {
        return application;
    }

    private FileRepository fileRepository;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        initializeInjection();
    }

    public FileRepository getFileRepository() {
        return fileRepository;
    }

    private void initializeInjection() {
        ApiManager apiManager = new ApiManagerImpl();
        this.fileRepository = new FileRepository(apiManager);
    }
}
