package com.jellyking.mimosa.comicsextractor.presentation.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.jellyking.mimosa.comicsextractor.R;
import com.jellyking.mimosa.comicsextractor.presentation.CEApplication;
import com.jellyking.mimosa.comicsextractor.presentation.composition.activity.dialog.DialogComponent;
import com.jellyking.mimosa.comicsextractor.presentation.composition.activity.dialog.DialogComponentImpl;
import com.jellyking.mimosa.comicsextractor.presentation.presenter.MainPresenter;
import com.jellyking.mimosa.comicsextractor.presentation.view.MainView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.activity_main_edittext_url)EditText editTextSourceUrl;
    @BindView(R.id.activity_main_edittext_digit_number)EditText editTextNumberOfDigit;
    @BindView(R.id.activity_main_edittext_first_index)EditText editTextFirstPageIndex;

    private MainPresenter presenter;
    private DialogComponent dialogComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initializeInjection();
    }

    @Override
    public void displayErrorMessage(String errorMessage) {
        dialogComponent.displayErrorMessage(errorMessage);
    }

    @Override
    public void displayIndeterminateProgressDialog(String title, String message) {
        dialogComponent.displayIndeterminateProgressDialog(title, message);
    }

    @Override
    public void updateDisplayedDialogWithMessage(String message) {
        dialogComponent.updateDisplayedDialogWithMessage(message);
    }

    @Override
    public void dismissDisplayedDialog() {
        dialogComponent.dismissDisplayedDialog();
    }

    @OnClick(R.id.activity_main_button_start)
    public void startDownload(){
        String message = validateFields();
        if(message == null){
            int numberOfDigit = (editTextNumberOfDigit.getText().toString().isEmpty()) ? 1 : Integer.valueOf(editTextNumberOfDigit.getText().toString());
            int indexOfTheFirstPage = (editTextFirstPageIndex.getText().toString().isEmpty()) ? 1 : Integer.valueOf(editTextFirstPageIndex.getText().toString());

            presenter.startDownload(editTextSourceUrl.getText().toString(), numberOfDigit, indexOfTheFirstPage);
        }else{
            displayErrorMessage(message);
        }
    }

    @OnClick(R.id.activity_main_button_demo)
    public void fillFieldsForDemo(){
        editTextSourceUrl.setText("http://www.scan-fr.net/uploads/manga/lastman-tome/chapters/1/{#}.jpg");
        editTextNumberOfDigit.setText("2");
        editTextFirstPageIndex.setText("1");
    }

    private String validateFields(){
        if(editTextSourceUrl.getText().toString().isEmpty()){
            return "Url field is empty";
        }

        if(!editTextSourceUrl.getText().toString().contains("{#}")) {
            return "Url field does not contain '{#}'";
        }

        return null;
    }

    private void initializeInjection(){
        this.dialogComponent = new DialogComponentImpl(this);
        this.presenter = new MainPresenter(this, CEApplication.application().getFileRepository());
    }
}
