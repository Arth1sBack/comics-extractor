package com.jellyking.mimosa.comicsextractor.presentation.composition.activity.dialog;

/**
 * Created by arthur on 11/03/2018.
 */

public interface DialogComponent {
    void displayErrorMessage(String errorMessage);
    void displayIndeterminateProgressDialog(String title, String message);
    void updateDisplayedDialogWithMessage(String message);
    void dismissDisplayedDialog();
}
