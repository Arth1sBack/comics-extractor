package com.jellyking.mimosa.comicsextractor.presentation.viewmodule;

/**
 * Created by arthur on 11/03/2018.
 */

public interface DialogViewModule {
    void displayErrorMessage(String errorMessage);
    void displayIndeterminateProgressDialog(String title, String message);
    void updateDisplayedDialogWithMessage(String message);
    void dismissDisplayedDialog();
}
