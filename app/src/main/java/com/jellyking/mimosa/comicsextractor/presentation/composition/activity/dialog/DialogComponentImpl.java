package com.jellyking.mimosa.comicsextractor.presentation.composition.activity.dialog;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;


/**
 * Created by arthur on 11/03/2018.
 */

public class DialogComponentImpl implements DialogComponent{

    final private Context context;

    private MaterialDialog dialog;

    public DialogComponentImpl(Context context){
        this.context = context;
    }

    @Override
    public void displayErrorMessage(String errorMessage){
        new MaterialDialog.Builder(context)
                .title("Error")
                .content(errorMessage)
                .positiveText("OK")
                .show();
    }

    @Override
    public void displayIndeterminateProgressDialog(String title, String message){
        dialog = new MaterialDialog.Builder(context)
                .title(title)
                .content(message)
                .cancelable(false)
                .progress(true, 0)
                .show();
    }

    @Override
    public void updateDisplayedDialogWithMessage(String message){
        if(dialog != null){
            dialog.setContent(message);
        }
    }

    @Override
    public void dismissDisplayedDialog(){
        if(dialog != null){
            dialog.dismiss();
            dialog = null;
        }
    }
}
