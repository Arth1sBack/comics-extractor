package com.jellyking.mimosa.comicsextractor.data.manager;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by arthur on 10/03/2018.
 */

public interface ApiManager {
    Observable<Response<ResponseBody>> download(String url);
}
